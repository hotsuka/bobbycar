#!/bin/bash

set -e

log() {
  echo
  echo "##### $*"
}

terminate() {
    log "$*"
    exit 1
}

wait_for_resource() {
  local namespace="$1"
  shift

  if [ "${namespace}" == "" ]; then
    namespace=${NAMESPACE}
  fi

  local timeout=$(($(date +%s) + 300))

  while ((timeout > $(date +%s))); do
    [[ "$(oc get -n "$namespace" "$@" -o 'go-template={{len .items}}' 2>/dev/null)" -gt 0 ]] && break
    sleep 5
  done

  if [[ ${timeout} < "$(date +%s)" ]]; then
      terminate "Error: timed out while waiting for '$*' (in namespace: $namespace) to exist."
  fi
}

# Wait for an operator to exist and be ready.
#
# $1 the label of the operator, for showing the user
# $2 the name of the operator resource (.spec.name from Subscription)
# $3 the namespace the operator will be installed
wait_for_operator() {
  local name="$1"
  local subscription="$2"
  local namespace="$3"

  if [ "${namespace}" == "" ]; then
    namespace=${NAMESPACE}
  fi

  local sel="deployment -l operators.coreos.com/${subscription}.${namespace}"

  log "Waiting for $name operator"
  # shellcheck disable=SC2086
  wait_for_resource $namespace $sel
  # shellcheck disable=SC2086
  oc -n "${namespace}" wait --for=condition=Available $sel --timeout=300s
}

source install_cleanup_vars.sh

# If weather api keys are empty, set a default value.
if [[ -z "$OWM_WEATHER_API_KEY" ]]; then
  OWM_WEATHER_API_KEY='my-owm-api-key';
fi;
if [[ -z "$IBM_WEATHER_API_KEY" ]]; then
  IBM_WEATHER_API_KEY='my-ibm-api-key';
fi;

if [[ "$INSTALL_KNATIVE" == true ]]; then
log "Installing OpenShift Serverless operator"
oc apply -f config/operators/serverless-operator-subscription.yaml
sleep 25
log "Waiting for OpenShift Serverless operator"
oc -n openshift-serverless wait --for=condition=Available deployment -l operators.coreos.com/serverless-operator.openshift-serverless --timeout=300s
log "Installing Knative Serving config"
oc apply -f config/knative/knative-serving.yaml
log "Installing Knative Eventing config"
oc apply -f config/knative/knative-eventing.yaml
log "Installing Knative Kafka config"
oc apply -f config/knative/knative-kafka.yaml
sleep 10
oc wait --for=condition=Ready knativeservings.v1beta1.operator.knative.dev/knative-serving --timeout 500s -n knative-serving
oc wait --for=condition=Ready knativeeventings.v1beta1.operator.knative.dev/knative-eventing --timeout 500s -n knative-eventing
oc wait --for=condition=Ready knativekafkas.v1alpha1.operator.serverless.openshift.io/knative-kafka --timeout 500s -n knative-eventing
log "OpenShift Serverless Installation has been completed!!"
fi ;

if [[ "$INSTALL_OPERATORS" == true ]]; then
log "Creating namespace $NAMESPACE for Bobbycar demo"
oc new-project "$NAMESPACE" > /dev/null 2>&1 || true
oc adm policy add-scc-to-user privileged -z default -n $NAMESPACE >/dev/null 2>&1 || true
log "Installing operator group"
sed "s:{{NAMESPACE}}:$NAMESPACE:g" config/operators/operator-group.yaml | oc apply -f -
log "Installing the AMQ Broker operator"
sed "s:{{NAMESPACE}}:$NAMESPACE:g" config/operators/amq-operator-subscription.yaml | oc apply -f -
log "Installing the AMQ Streams operator"
sed "s:{{NAMESPACE}}:$NAMESPACE:g" config/operators/amq-streams-operator-subscription.yaml | oc apply -f -
log "Installing the Datagrid operator"
sed "s:{{NAMESPACE}}:$NAMESPACE:g" config/operators/datagrid-subscription.yaml | oc apply -f -
log "Installing the Camel-K operator"
sed "s:{{NAMESPACE}}:$NAMESPACE:g" config/operators/camel-k-operator-subscription.yaml | oc apply -f -
log "Installing the OpenShift Pipelines operator"
oc apply -f config/operators/pipeline-operator-subscription.yaml
wait_for_operator "AMQ Broker" amq-broker-rhel8
wait_for_operator "AMQ Streams" amq-streams
wait_for_operator "Datagrid" datagrid
wait_for_operator "Camel-K" red-hat-camel-k
wait_for_operator "Pipelines" openshift-pipelines-operator-rh openshift-operators

fi ;

log "Installing the infra Helm release: $HELM_INFRA_RELEASE_NAME"
helm upgrade --install "$HELM_INFRA_RELEASE_NAME" --set-string namespace="$NAMESPACE" --set-string ocpDomain="$APP_DOMAIN" helm/bobbycar-core-infra/

sleep 30

log "Waiting for AMQ Broker pod"
oc wait --for=condition=Ready pod/bobbycar-amq-mqtt-ss-0 --timeout 300s
log "Waiting for Datagrid pod"
oc wait --for=condition=Ready pod/bobbycar-dg-0 --timeout 300s
log "Waiting for Kafka Broker pod"
oc wait --for=condition=Ready pod/bobbycar-cluster-kafka-0 --timeout 300s
log "Waiting for Kafka Bridge pod"
oc wait --for=condition=Available deployment/bobbycar-bridge --timeout 300s


log "Installing the apps Helm release: $HELM_APP_RELEASE_NAME"
helm upgrade --install "$HELM_APP_RELEASE_NAME" helm/bobbycar-core-apps \
--set-string ocpDomain="$APP_DOMAIN" \
--set-string ocpApi="$API_DOMAIN" \
--set-string namespace="$NAMESPACE" \
--set-string dashboard.config.googleApiKey="$GOOGLE_API_KEY" \
--set-string weatherService.owm.api.key="$OWM_WEATHER_API_KEY" \
--set-string weatherService.ibm.api.key="$IBM_WEATHER_API_KEY" \
--set-string dashboard.config.ocpApiUrl="https://$API_DOMAIN:6443"

sleep 30

log "Waiting for Bobbycar pod"
oc wait --for=condition=Available deployment/car-simulator --timeout 300s
log "Waiting for Bobbycar Dashboard pod"
oc wait --for=condition=Available dc/dashboard --timeout 300s
log "Waiting for Dashboard Streaming service pod"
oc wait --for=condition=Available deployment/dashboard-streaming --timeout 300s

log "Installing the Serverless Helm release: $HELM_SERVERLESS_RELEASE_NAME"
helm upgrade --install "$HELM_SERVERLESS_RELEASE_NAME" helm/bobbycar-opt-serverless \
--set-string namespace="$NAMESPACE" \
--set-string otaServer.url="http://ota-server-bobbycar.$APP_DOMAIN"

log "Waiting for Camel-K integrations to complete..."
oc wait --for=condition=Ready integration/cache-service --timeout 1800s
oc wait --for=condition=Ready integration/kafka2datagrid --timeout 1800s
oc wait --for=condition=Ready integration/mqtt2kafka --timeout 1800s

### dev ###

set +e
log "Start installing dev-related components."

HELM_DEV_BASE_RELEASE_NAME=dev-base
helm upgrade --install "$HELM_DEV_BASE_RELEASE_NAME" helm/bobbycar-opt-dev-base

log "Temporarily installing helm/bobbycar-opt-dev-base to determine domains..."
while true; do
  GOGS_DOMAIN=$(oc get route/gogs -o jsonpath='{.spec.host}') && \
  NEXUS3_DOMAIN=$(oc get route/nexus3 -o jsonpath='{.spec.host}') && \
  SONARQUBE_DOMAIN=$(oc get route/sonarqube -o jsonpath='{.spec.host}')
  if [ $? -eq 0 ]; then
    break
  fi
  sleep 3
done
log "done."

log "Deleting temporary helm/bobbycar-opt-dev-base..."
helm delete "$HELM_DEV_BASE_RELEASE_NAME"

while true; do
  sleep 3
  oc get pods | grep gogs > /dev/null 2>&1
  if [ $? -eq 0 ]; then continue; fi
  oc get pods | grep nexus > /dev/null 2>&1
  if [ $? -eq 0 ]; then continue; fi
  oc get pods | grep sonarqube > /dev/null 2>&1
  if [ $? -eq 0 ]; then continue; fi
  break
done
log "done."

log "Temporarily installing helm/bobbycar-opt-dev-pipelines to determine Tekton EventListener URL..."
HELM_DEV_PIPELINE_RELEASE_NAME=dev-pipeline
helm upgrade --install "$HELM_DEV_PIPELINE_RELEASE_NAME" helm/bobbycar-opt-dev-pipelines \
--set-string pipeline.bobbycar.maven.mirror="http://${NEXUS3_DOMAIN}/repository/maven-public/" \
--set-string pipeline.bobbycar.gitCloneUrl="http://${GOGS_DOMAIN}/gogs/bobbycar.git" \
--set-string pipeline.bobbycar.sonar.host="http://${SONARQUBE_DOMAIN}/" \
--set-string pipeline.bobbycar.nexus.snapshotRepoUrl="http://${NEXUS3_DOMAIN}/repository/maven-snapshots/" \
--set-string pipeline.bobbycar.nexus.releaseRepoUrl="http://${NEXUS3_DOMAIN}/repository/maven-releases/"

while true; do
  tekton_el=$(oc get el/car-sim-triggerref -o jsonpath='{.status.address.url}')
  if [ $? -eq 0 ]; then
    break
  fi
  sleep 3
done
log "done."

log "Deleting temporary helm/bobbycar-opt-dev-pipelines..."
oc get pod | grep car-sim-run | awk '{print $1}' | xargs oc delete pod
oc get pod | grep carsim-pipe | awk '{print $1}' | xargs oc delete pod
helm delete "$HELM_DEV_PIPELINE_RELEASE_NAME"
while true; do
  oc get pvc | grep carsim-pipe-pvc >/dev/null
  if [ $? -ne 0 ]; then
    break
  fi
  sleep 3
done
log "done."

log "Installing helm/bobbycar-opt-dev-base..."
helm upgrade --install "$HELM_DEV_BASE_RELEASE_NAME" helm/bobbycar-opt-dev-base

oc wait --for=condition=Available dc/postgresql
oc wait --for=condition=Available deployment/gogs
log "done."

log "Configuring gogs..."
while true; do
  oc exec deployment/gogs -- mkdir -p /opt/gogs/custom/conf/ >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    break
  fi
  sleep 3
done
cat app.ini_ | sed -e "s/{{GOGS_DOMAIN}}/${GOGS_DOMAIN}/g" > app.ini
oc cp app.ini $(oc get pod -l app=gogs -o jsonpath="{.items[0].metadata.name}"):/opt/gogs/custom/conf/app.ini
rm -f app.ini
log "done."

log "Restarting gogs ..."
oc scale deployment/gogs --replicas 0
sleep 15
oc scale deployment/gogs --replicas 1
log "done."

log "Creating gogs user in gogs..."
while true; do
  oc exec deployment/gogs -- env USER=gogs /opt/gogs/gogs admin create-user --admin --name gogs --password gogs --email gogs@example.com >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    break
  fi
  sleep 3
done
log "done."

log "Registering bobbycar repository and its webhook in gogs..."
while true; do
  gogs_token=$(curl -sS -u gogs:gogs -X POST -H "Content-Type: application/json" http://${GOGS_DOMAIN}/api/v1/users/gogs/tokens -d '{"name": "gogs"}' | jq .sha1 -r)
  if [ $? -eq 0 ]; then
    break
  fi
  sleep 3
done
curl -sS -X POST -H "Authorization: token ${gogs_token}" -H "Content-Type: application/json" http://${GOGS_DOMAIN}/api/v1/admin/users/gogs/repos -d '{"name": "bobbycar", "private": false}'
(cd /tmp; \
rm -rf bobbycar; \
git clone https://github.com/sa-mw-dach/bobbycar; \
cd bobbycar; \
git remote add gogs http://gogs:gogs@${GOGS_DOMAIN}/gogs/bobbycar; \
git push -f gogs master)
curl -sS -X POST -H "Authorization: token ${gogs_token}" -H "Content-Type: application/json" http://${GOGS_DOMAIN}/api/v1/repos/gogs/bobbycar/hooks -d '{"type":"gogs","config":{"content_type":"json","url":"'${tekton_el}'"},"events":["push"],"active":true}'
log "done."

log "Generating SonarQube token..."
while true; do
  sonarqube_token=$(curl -sS -u admin:admin -X POST -H "Content-Type: application/x-www-form-urlencoded" http://${SONARQUBE_DOMAIN}/api/user_tokens/generate?name=bobbycar | jq .token -r) > /dev/null 2>&1
  if [ "${sonarqube_token}" != "null" -a "${sonarqube_token}" != "" ]; then
    break
  fi
  sleep 10
  echo -n "."
done
log "done."

log "Retrieving Nexus credentials..."
while true; do
  nexus_password=$(oc exec -it dc/nexus3 -- cat /nexus-data/admin.password)
  if [ $? -eq 0 -a "${nexus_password}" != "" ]; then
    break
  fi
  sleep 3
done
log "done."

log "Installing helm/bobbycar-opt-dev-pipelines..."
helm upgrade --install "$HELM_DEV_PIPELINE_RELEASE_NAME" helm/bobbycar-opt-dev-pipelines \
--set-string pipeline.bobbycar.maven.mirror="http://${NEXUS3_DOMAIN}/repository/maven-public/" \
--set-string pipeline.bobbycar.gitCloneUrl="http://${GOGS_DOMAIN}/gogs/bobbycar.git" \
--set-string pipeline.bobbycar.sonar.host="http://${SONARQUBE_DOMAIN}/" \
--set-string pipeline.bobbycar.nexus.snapshotRepoUrl="http://${NEXUS3_DOMAIN}/repository/maven-snapshots/" \
--set-string pipeline.bobbycar.nexus.releaseRepoUrl="http://${NEXUS3_DOMAIN}/repository/maven-releases/" \
--set-string pipeline.bobbycar.sonar.loginToken="${sonarqube_token}" \
--set-string pipeline.bobbycar.maven.repoRelease.password="${nexus_password}" \
--set-string pipeline.bobbycar.maven.repoSnapshot.password="${nexus_password}" \
--set-string pipeline.bobbycar.containerImage="image-registry.openshift-image-registry.svc:5000/${NAMESPACE}/car-simulator"

log "done."

log "Patching pipelines-scc..."
while true; do
  oc patch scc pipelines-scc --type merge -p '{"allowedCapabilities":["SETFCAP"]}'
  if [ $? -eq 0 ]; then
    break
  fi
  sleep 3
done
log "done."

log "Installation completed! Open the Bobbycar dashboard and get started:"
echo "http://$(oc get route dashboard -o json | jq -r .spec.host)/"
echo "Gogs:"
echo "http://${GOGS_DOMAIN}/"
echo "Username/Password: gogs/gogs"
echo ""
echo "SonarQube:"
echo "http://${SONARQUBE_DOMAIN}/"
echo "Username/Password: admin/admin"
echo ""
echo "Nexus:"
echo "http://${NEXUS3_DOMAIN}/"
echo "Username/Password: admin/${nexus_password}"


