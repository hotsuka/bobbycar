#!/bin/bash

if [ "$(oc whoami)" != "admin" -a "$(oc whoami)" != "opentlc-mgr" ]; then
  echo "Login as admin or opentlc-mgr first."
  exit
fi

. install_cleanup_vars.sh

APP_DOMAIN=$(oc get ingresses.config/cluster -o jsonpath={.spec.domain})
API_DOMAIN=$(oc whoami --show-server | sed -e "s|^.*//\([a-z0-9\.\-][a-z0-9\.\-]*\):.*$|\1|g")

if [ "${NAMESPACE}" == "" ]; then
  NAMESPACE=bobbycar
fi
if [ "${GOOGLE_API_KEY}" == "" ]; then
  GOOGLE_API_KEY=dummy
fi
if [ "${OWM_WEATHER_API_KEY}" == "" ]; then
  OWM_WEATHER_API_KEY=dummy
fi
if [ "${IBM_WEATHER_API_KEY}" == "" ]; then
  IBM_WEATHER_API_KEY=dummy
fi

echo -n "NAMESPACE (current: ${NAMESPACE}): "
read
if [ "${REPLY}" != "" ]; then
  NAMESPACE=${REPLY}
fi

echo -n "GOOGLE_API_KEY (current: ${GOOGLE_API_KEY}): "
read
if [ "${REPLY}" != "" ]; then
  GOOGLE_API_KEY=${REPLY}
fi

echo -n "OWM_WEATHER_API_KEY (current: ${OWM_WEATHER_API_KEY}): "
read
if [ "${REPLY}" != "" ]; then
  OWM_WEATHER_API_KEY=${REPLY}
fi
echo -n "IBM_WEATHER_API_KEY (current: ${IBM_WEATHER_API_KEY}): "
read
if [ "${REPLY}" != "" ]; then
  IBM_WEATHER_API_KEY=${REPLY}
fi

sed -i "s/^NAMESPACE=.*$/NAMESPACE=${NAMESPACE}/g" install_cleanup_vars.sh
sed -i "s/^APP_DOMAIN=.*$/APP_DOMAIN=${APP_DOMAIN}/g" install_cleanup_vars.sh
sed -i "s/^API_DOMAIN=.*$/API_DOMAIN=${API_DOMAIN}/g" install_cleanup_vars.sh
sed -i "s/^GOOGLE_API_KEY=.*$/GOOGLE_API_KEY=${GOOGLE_API_KEY}/g" install_cleanup_vars.sh
sed -i "s/^OWM_WEATHER_API_KEY=.*$/OWM_WEATHER_API_KEY=${OWM_WEATHER_API_KEY}/g" install_cleanup_vars.sh
sed -i "s/^IBM_WEATHER_API_KEY=.*$/IBM_WEATHER_API_KEY=${IBM_WEATHER_API_KEY}/g" install_cleanup_vars.sh

if [ ! -f /usr/local/bin/helm ]; then
  echo ""
  echo "##### Installing Helm"
  curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
  chmod 700 get_helm.sh
  ./get_helm.sh
  rm -f get_helm.sh
fi

while true; do
  ./_install.sh
  if [ $? -eq 0 ]; then
    break
  fi
  echo "Retrying ..."
  sleep 3
done
